export interface NewPost {
    id: number | null;
    authorId: number;
    body: string;
    previewImage: string;
}
