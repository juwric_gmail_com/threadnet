import { Component, Inject, OnInit } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
    selector: "app-likes-dialog",
    templateUrl: "./likes-dialog.component.html",
    styleUrls: ["./likes-dialog.component.sass"],
})
export class LikesDialogComponent implements OnInit {
    constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}

    ngOnInit(): void {}

    onImgError(event) {
        event.target.src = "./assets/blank.png";
    }
}
