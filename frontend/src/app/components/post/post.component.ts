import {
    Component,
    EventEmitter,
    Input,
    OnDestroy,
    Output,
} from "@angular/core";
import { Post } from "../../models/post/post";
import { AuthenticationService } from "../../services/auth.service";
import { AuthDialogService } from "../../services/auth-dialog.service";
import { empty, Observable, Subject } from "rxjs";
import { DialogType } from "../../models/common/auth-dialog-type";
import { LikeService } from "../../services/like.service";
import { NewComment } from "../../models/comment/new-comment";
import { CommentService } from "../../services/comment.service";
import { User } from "../../models/user";
import { Comment } from "../../models/comment/comment";
import { catchError, switchMap, takeUntil } from "rxjs/operators";
import { SnackBarService } from "../../services/snack-bar.service";
import { PostService } from "src/app/services/post.service";
import { Reaction } from "src/app/models/reactions/reaction";

@Component({
    selector: "app-post",
    templateUrl: "./post.component.html",
    styleUrls: ["./post.component.sass"],
})
export class PostComponent implements OnDestroy {
    @Input() public post: Post;
    @Input() public currentUser: User;
    @Output() onPostChange = new EventEmitter<Post>();
    @Output() onPostDelete = new EventEmitter<Post>();

    public showComments = false;
    public newComment = {} as NewComment;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService
    ) {}

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    get likes() {
        return this.post.reactions.filter((r) => r.isLike);
    }

    public updatePost() {
        this.onPostChange.emit(this.post);
    }

    public onDeleteComment(comment: Comment) {
        this.post.comments = this.post.comments.filter(
            (c) => c.id !== comment.id
        );
    }

    public deletePost() {
        this.onPostDelete.emit(this.post);
    }

    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }

        this.showComments = !this.showComments;
    }

    public postLikesLen() {
        return this.post.reactions.filter((q) => q.isLike === true).length;
    }

    public postDislikesLen() {
        return this.post.reactions.filter((q) => q.isLike === false).length;
    }

    public likePost() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) =>
                        this.likeService.likePost(this.post, userResp)
                    ),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));

            return;
        }

        this.likeService
            .likePost(this.post, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.post = post));
    }

    public dislikePost() {
        this.catchErrorWrapper(this.authService.getUser())
            .pipe(
                switchMap((userResp: User) =>
                    this.likeService.likePost(this.post, userResp, false)
                ),
                takeUntil(this.unsubscribe$)
            )
            .subscribe((post) => (this.post = post));
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(
                            this.post.comments.concat(resp.body)
                        );
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort(
            (a, b) => +new Date(b.createdAt) - +new Date(a.createdAt)
        );
    }

    onImgError(event) {
        event.target.src = "./assets/blank.png";
    }
}
