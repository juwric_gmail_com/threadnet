import { Component, Input, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Reaction } from "src/app/models/reactions/reaction";
import { LikesDialogComponent } from "../likes-dialog/likes-dialog.component";

@Component({
    selector: "app-likes",
    templateUrl: "./likes.component.html",
    styleUrls: ["./likes.component.sass"],
})
export class LikesComponent implements OnInit {
    @Input() public likes: Reaction[];
    constructor(private dialog: MatDialog) {}

    ngOnInit(): void {}

    onImgError(event) {
        event.target.src = "./assets/blank.png";
    }

    public showAllLikes() {
        this.dialog.open(LikesDialogComponent, {
            data: this.likes,
            minWidth: 300,
            autoFocus: true,
            backdropClass: "dialog-backdrop",
            position: {
                top: "0",
            },
        });
    }
}
