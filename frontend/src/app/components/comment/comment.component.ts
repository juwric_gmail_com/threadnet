import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { DialogType } from "src/app/models/common/auth-dialog-type";
import { User } from "src/app/models/user";
import { AuthDialogService } from "src/app/services/auth-dialog.service";
import { AuthenticationService } from "src/app/services/auth.service";
import { CommentService } from "src/app/services/comment.service";
import { LikeService } from "src/app/services/like.service";
import { SnackBarService } from "src/app/services/snack-bar.service";
import { Comment } from "../../models/comment/comment";
import { MatDialog } from "@angular/material/dialog";
import { LikesDialogComponent } from "../likes-dialog/likes-dialog.component";

@Component({
    selector: "app-comment",
    templateUrl: "./comment.component.html",
    styleUrls: ["./comment.component.sass"],
})
export class CommentComponent {
    @Input() public comment: Comment;
    @Output() onDeleteComment = new EventEmitter<Comment>();

    public authorizedUser: User;
    public isUpdate: Boolean = false;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private authService: AuthenticationService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private likeService: LikeService,
        private authDialogService: AuthDialogService,
        private dialog: MatDialog
    ) {}

    public ngOnInit() {
        this.authService
            .getUser()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((user) => (this.authorizedUser = { ...user }));
    }

    public updateComment() {
        this.isUpdate = !this.isUpdate;
    }

    public lenLikes() {
        return this.comment.reactions.filter((c) => c.isLike === true).length;
    }

    public lenDislikes() {
        return this.comment.reactions.filter((c) => c.isLike === false).length;
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public likeComment() {
        if (this.authorizedUser) {
            this.likeService
                .likeComment(this.comment, this.authorizedUser)
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe(
                    (resp) => {},
                    (error) =>
                        this.authDialogService.openAuthDialog(DialogType.SignIn)
                );
        } else {
            this.authDialogService.openAuthDialog(DialogType.SignIn);
        }
    }

    public showAllLikes() {
        this.dialog.open(LikesDialogComponent, {
            data: this.comment.reactions.filter((r) => r.isLike),
            minWidth: 300,
            autoFocus: true,
            backdropClass: "dialog-backdrop",
            position: {
                top: "0",
            },
        });

        return false;
    }

    public dislikeComment() {
        if (this.authorizedUser) {
            this.likeService
                .likeComment(this.comment, this.authorizedUser, false)
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe(
                    (resp) => {},
                    (error) =>
                        this.authDialogService.openAuthDialog(DialogType.SignIn)
                );
        } else {
            this.authDialogService.openAuthDialog(DialogType.SignIn);
        }
    }

    public sendComment() {
        this.isUpdate = false;

        this.commentService
            .updateComment(this.comment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {},
                (error) =>
                    this.authDialogService.openAuthDialog(DialogType.SignIn)
            );
    }

    public deleteComment() {
        this.commentService
            .deleteComment(this.comment.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    this.onDeleteComment.emit(this.comment);
                },
                (error) =>
                    this.authDialogService.openAuthDialog(DialogType.SignIn)
            );
    }
}
