import { Comment } from "./../models/comment/comment";
import { Injectable } from "@angular/core";
import { AuthenticationService } from "./auth.service";
import { Post } from "../models/post/post";
import { NewReaction } from "../models/reactions/newReaction";
import { PostService } from "./post.service";
import { User } from "../models/user";
import { map, catchError } from "rxjs/operators";
import { of } from "rxjs";
import { HttpInternalService } from "./http-internal.service";
import { CommentService } from "./comment.service";

@Injectable({ providedIn: "root" })
export class LikeService {
    public constructor(
        private authService: AuthenticationService,
        private postService: PostService,
        private commentService: CommentService,
        private httpService: HttpInternalService
    ) {}

    public likePost(post: Post, currentUser: User, isLike: boolean = true) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike,
            userId: currentUser.id,
        };

        // update current array instantly
        let hasReaction = innerPost.reactions.some(
            (x) => x.user.id === currentUser.id
        );
        innerPost.reactions = hasReaction
            ? innerPost.reactions.filter((x) => x.user.id !== currentUser.id)
            : innerPost.reactions.concat({ isLike, user: currentUser });
        hasReaction = innerPost.reactions.some(
            (x) => x.user.id === currentUser.id
        );

        return this.postService.likePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                // revert current array changes in case of any error
                innerPost.reactions = hasReaction
                    ? innerPost.reactions.filter(
                          (x) => x.user.id !== currentUser.id
                      )
                    : innerPost.reactions.concat({
                          isLike,
                          user: currentUser,
                      });

                return of(innerPost);
            })
        );
    }

    public likeComment(
        comment: Comment,
        currentUser: User,
        isLike: boolean = true
    ) {
        const reaction: NewReaction = {
            entityId: comment.id,
            isLike,
            userId: currentUser?.id,
        };

        let hasReaction = comment.reactions.some(
            (x) => x.user?.id === currentUser?.id
        );

        comment.reactions = hasReaction
            ? comment.reactions.filter((x) => x.user?.id !== currentUser?.id)
            : comment.reactions.concat({ isLike, user: currentUser });

        return this.commentService.likeComment(reaction);
    }
}
