﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class AddDeletedAtForComment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedAt",
                table: "Comments",
                type: "datetime2",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Iusto et aut doloribus ut debitis.", new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(5322), 12, new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(6250) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { "Sed sit cum nobis expedita et id molestiae nihil qui.", new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(7277), new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(7296) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Itaque accusantium facilis expedita et.", new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(7422), 11, new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(7433) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Enim consectetur ex.", new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(7635), 5, new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(7649) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Et quaerat fugit aperiam dolores ut voluptatum.", new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(7782), 11, new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(7794) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Corrupti voluptatibus dolorum dicta.", new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(7895), 18, new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(7906) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Fuga repudiandae ipsum error omnis ut culpa magnam voluptates.", new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(8029), 18, new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(8040) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Eos voluptatem modi blanditiis inventore odit nam enim aut.", new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(8164), 4, new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(8175) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Qui molestias molestiae dolorem facilis molestiae qui soluta et.", new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(8294), 15, new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(8305) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Doloremque repellat pariatur repudiandae provident dolor officia.", new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(8480), 13, new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(8494) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Vel eveniet voluptatem non error reiciendis eligendi similique omnis magni.", new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(8630), 8, new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(8642) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Enim sint velit id deleniti id.", new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(8749), 8, new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(8760) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Qui tempore nihil inventore aliquam facere omnis voluptas ut.", new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(8876), 4, new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(8887) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Consequuntur provident ut sed.", new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(8979), 10, new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(8990) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Eos natus animi illum facilis in culpa asperiores quia dolor.", new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(9186), 14, new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(9200) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Et natus enim deserunt aut quas.", new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(9307), 19, new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(9318) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Et incidunt voluptas modi ut qui eum molestias atque.", new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(9436), 3, new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(9446) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 17, "Excepturi quaerat maiores at.", new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(9538), new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(9549) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Possimus expedita voluptatem autem sed impedit sed similique.", new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(9665), 7, new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(9676) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Commodi facere illo.", new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(9763), 3, new DateTime(2022, 1, 10, 10, 35, 47, 514, DateTimeKind.Local).AddTicks(9774) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 202, DateTimeKind.Local).AddTicks(5422), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/379.jpg", new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(5356) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(6895), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/875.jpg", new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(6933) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(7007), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/708.jpg", new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(7015) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(7661), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/653.jpg", new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(7687) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(7761), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1102.jpg", new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(7770) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(7806), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1009.jpg", new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(7814) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(7847), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1145.jpg", new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(7854) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(7887), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/415.jpg", new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(7894) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(7925), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/16.jpg", new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(7933) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(7964), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/932.jpg", new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(7972) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(8001), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1102.jpg", new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(8009) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(8039), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/630.jpg", new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(8047) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(8076), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/718.jpg", new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(8084) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(8114), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/529.jpg", new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(8122) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(8153), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/583.jpg", new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(8161) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(8191), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/216.jpg", new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(8198) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(8228), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/393.jpg", new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(8236) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(8266), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/430.jpg", new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(8273) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(8304), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/150.jpg", new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(8311) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(8341), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/248.jpg", new DateTime(2022, 1, 10, 10, 35, 47, 203, DateTimeKind.Local).AddTicks(8349) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 214, DateTimeKind.Local).AddTicks(9691), "https://picsum.photos/640/480/?image=999", new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(737) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(1127), "https://picsum.photos/640/480/?image=30", new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(1139) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(1183), "https://picsum.photos/640/480/?image=758", new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(1191) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(1226), "https://picsum.photos/640/480/?image=106", new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(1234) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(1855), "https://picsum.photos/640/480/?image=268", new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(1881) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(1945), "https://picsum.photos/640/480/?image=281", new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(1953) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(1988), "https://picsum.photos/640/480/?image=150", new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(1997) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(2031), "https://picsum.photos/640/480/?image=41", new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(2039) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(2074), "https://picsum.photos/640/480/?image=992", new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(2082) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(2116), "https://picsum.photos/640/480/?image=694", new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(2124) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(2158), "https://picsum.photos/640/480/?image=239", new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(2165) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(2199), "https://picsum.photos/640/480/?image=877", new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(2207) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(2241), "https://picsum.photos/640/480/?image=490", new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(2249) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(2282), "https://picsum.photos/640/480/?image=822", new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(2290) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(2324), "https://picsum.photos/640/480/?image=751", new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(2332) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(2366), "https://picsum.photos/640/480/?image=867", new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(2374) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(2407), "https://picsum.photos/640/480/?image=418", new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(2415) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(2909), "https://picsum.photos/640/480/?image=504", new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(2933) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(2991), "https://picsum.photos/640/480/?image=894", new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(2999) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(3034), "https://picsum.photos/640/480/?image=446", new DateTime(2022, 1, 10, 10, 35, 47, 215, DateTimeKind.Local).AddTicks(3043) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Recusandae libero explicabo alias ut officia laboriosam doloribus.", new DateTime(2022, 1, 10, 10, 35, 47, 502, DateTimeKind.Local).AddTicks(1688), 25, new DateTime(2022, 1, 10, 10, 35, 47, 502, DateTimeKind.Local).AddTicks(2669) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Quam numquam pariatur unde quisquam error ex est.", new DateTime(2022, 1, 10, 10, 35, 47, 502, DateTimeKind.Local).AddTicks(3860), 39, new DateTime(2022, 1, 10, 10, 35, 47, 502, DateTimeKind.Local).AddTicks(3881) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "libero", new DateTime(2022, 1, 10, 10, 35, 47, 502, DateTimeKind.Local).AddTicks(4557), 37, new DateTime(2022, 1, 10, 10, 35, 47, 502, DateTimeKind.Local).AddTicks(4581) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Nemo molestiae ut sint qui non ab.", new DateTime(2022, 1, 10, 10, 35, 47, 502, DateTimeKind.Local).AddTicks(4909), 29, new DateTime(2022, 1, 10, 10, 35, 47, 502, DateTimeKind.Local).AddTicks(4926) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Sunt doloribus deserunt sint et aut aut quam possimus. Quasi tenetur porro deserunt. Libero non molestiae dolor quis adipisci quo nobis. Saepe culpa labore corrupti dolorem.", new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(305), 29, new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(335) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Nobis hic tempore et ab porro. Ut maxime aut. Enim sed eos velit vel sint rerum cumque fugit.", new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(811), 34, new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(830) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "Dolorum eaque officiis occaecati aliquam.", new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(1004), 28, new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(1018) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "temporibus", new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(1095), 25, new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(1106) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Autem laudantium ut tempora sunt in molestiae culpa autem vel. Repellendus atque tempora qui ab. Et facilis ea consequatur perferendis voluptas provident porro reprehenderit. Fuga cupiditate aut. Quam eum sit sunt ut aut qui nesciunt qui aut. Distinctio quos incidunt.", new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(4113), 30, new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(4142) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Quam voluptatum dolores quidem saepe ea dolorum cumque. Cumque quos aut inventore in molestiae officiis. Totam expedita et ipsum ea enim.", new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(4547), 39, new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(4564) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "Corporis magnam occaecati quis sed. Iste voluptatum veniam placeat quia non et dolor earum. Labore beatae reiciendis quaerat et ea. Ea qui dolor.", new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(4815), 32, new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(4827) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "numquam", new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(4899), 24, new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(4910) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "enim", new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(5073), 29, new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(5087) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "sed", new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(5163), 26, new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(5173) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Et nisi dignissimos nostrum doloribus esse vel. In asperiores explicabo veniam earum sit fugit qui. Non dolores enim et officia. Sed nisi omnis nobis. Aperiam vel cumque esse fugit. Velit inventore optio necessitatibus culpa facilis reprehenderit sed omnis.", new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(5600), 39, new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(5616) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "Quasi veritatis voluptates. Aliquid soluta occaecati. Ratione et rerum. Qui ut est officia.", new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(5821), 26, new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(5834) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "quia", new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(5905), 36, new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(5916) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Harum pariatur debitis provident eum ut.", new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(6025), 26, new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(6037) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Quia aut ut quidem veritatis et dolore.", new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(6203), 27, new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(6216) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "Consequatur repudiandae at necessitatibus quia rerum labore qui veniam corporis.", new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(6361), 21, new DateTime(2022, 1, 10, 10, 35, 47, 503, DateTimeKind.Local).AddTicks(6374) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2022, 1, 10, 10, 35, 47, 278, DateTimeKind.Local).AddTicks(1570), "Matt5@yahoo.com", "49VunSWyz9u49K4X7xd8oq0Kq2eC+Zw7DQmm/ZIiC3A=", "hBrIHDeOv2A8HtBPRgyFD6kyOzHWbI01zoq0F1UPsuI=", new DateTime(2022, 1, 10, 10, 35, 47, 278, DateTimeKind.Local).AddTicks(2903), "Zack.Cole" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2022, 1, 10, 10, 35, 47, 289, DateTimeKind.Local).AddTicks(4389), "Patricia_Orn@gmail.com", "duv1CIbWwn+vHWUj2M85y84R+eUX3XMFfv+C6VujMgk=", "QgbudOL+2fxMscBe+WkZP1Zcc8xOrOHsnoAD60PK+6E=", new DateTime(2022, 1, 10, 10, 35, 47, 289, DateTimeKind.Local).AddTicks(4477), "Paris_Stanton" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2022, 1, 10, 10, 35, 47, 299, DateTimeKind.Local).AddTicks(6506), "Dedric.Marvin@yahoo.com", "wyGM8vh3zZWS/4WYJCEEfepBWNHhxzKaQuL9YTUBBvI=", "yCJt9miDb2cWWXzyPlFSD0iiWQ0mLqxz3mkWLX/ByWM=", new DateTime(2022, 1, 10, 10, 35, 47, 299, DateTimeKind.Local).AddTicks(6590), "Micaela.Yundt" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2022, 1, 10, 10, 35, 47, 310, DateTimeKind.Local).AddTicks(6065), "Santino.Strosin@gmail.com", "jiWbqJgcrKs5j4i0J0xaRiXIX3V8mMns6ayhM7ThRCc=", "f5DyP1t1Jpp1BqcQYYHms8V4y0bS806UhdVnkAxaqTY=", new DateTime(2022, 1, 10, 10, 35, 47, 310, DateTimeKind.Local).AddTicks(6155), "Darwin.Robel" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2022, 1, 10, 10, 35, 47, 321, DateTimeKind.Local).AddTicks(494), "Tiara_Gerlach@hotmail.com", "d2kM2tMfzIodiYNtMejRLJqZix8ILsNsz5GUbvnCZ4M=", "6Ol7m0OW2bANoBwCAOAtA+knAlSxguIXqrINE79dpe4=", new DateTime(2022, 1, 10, 10, 35, 47, 321, DateTimeKind.Local).AddTicks(582), "Tressie73" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2022, 1, 10, 10, 35, 47, 331, DateTimeKind.Local).AddTicks(3788), "Kaleigh.Beer67@gmail.com", "ilw8LJJ1OkAnS7kq3CpCeH3KKLLJ8wM+6+ATi7U9WmQ=", "pwbumVXUoPXYPU/WFWSmC9zSGMEUYytCmGjD0m50wzA=", new DateTime(2022, 1, 10, 10, 35, 47, 331, DateTimeKind.Local).AddTicks(3868), "Edmond_Labadie" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2022, 1, 10, 10, 35, 47, 341, DateTimeKind.Local).AddTicks(9422), "Kay.Roberts25@yahoo.com", "Wwbo8ZypNP5ymqnW0OStkge4nq8YlPkm1qLsamr2A9Y=", "Xu9q8J3x50N8XzMhBdtBwIjQZEOQEbWQxZlw2CMTWgU=", new DateTime(2022, 1, 10, 10, 35, 47, 341, DateTimeKind.Local).AddTicks(9511), "Skyla.Nikolaus74" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2022, 1, 10, 10, 35, 47, 352, DateTimeKind.Local).AddTicks(8900), "Delilah_Willms23@yahoo.com", "M2QeuPHfhiE9FJwOBpx8sqdP50a/8ukEY/xJZJBo4H0=", "o30Ry/VpMDZ//9lr2TwxnGI2+6wXNPvIyEt3COhIDNY=", new DateTime(2022, 1, 10, 10, 35, 47, 352, DateTimeKind.Local).AddTicks(9005), "Reinhold_Nitzsche2" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2022, 1, 10, 10, 35, 47, 363, DateTimeKind.Local).AddTicks(2917), "Gordon0@yahoo.com", "l2D06qv9Bhd4WrlWZTESoXVdjzIRSRHZcEmbRfSOyuk=", "942C4n5JuIGDAM5Lf/4hogvE0dWTlgjQce//dto7f+Q=", new DateTime(2022, 1, 10, 10, 35, 47, 363, DateTimeKind.Local).AddTicks(3002), "Enos0" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2022, 1, 10, 10, 35, 47, 373, DateTimeKind.Local).AddTicks(4479), "Mitchel.Christiansen25@gmail.com", "XSOs5bezMfJ/msBF0e5/VHrMhWlvAiD28hpx5VFxrwo=", "S3S3+msVMQ5qAF/YRCbrgOseoXZjwv56nantg07oLKk=", new DateTime(2022, 1, 10, 10, 35, 47, 373, DateTimeKind.Local).AddTicks(4547), "Trystan74" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2022, 1, 10, 10, 35, 47, 383, DateTimeKind.Local).AddTicks(6421), "Cleora.Pagac70@hotmail.com", "fzw54VeqRVgF6P12KYNVXfJJtxI34rt8TM7e8pm2A0Y=", "brg/NaVwtIbSc9NExweE3AWBMxTS3MRw+RzJ/aiDmBc=", new DateTime(2022, 1, 10, 10, 35, 47, 383, DateTimeKind.Local).AddTicks(6485), "Shawn61" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2022, 1, 10, 10, 35, 47, 393, DateTimeKind.Local).AddTicks(6214), "Karlie27@yahoo.com", "1sFrHNrcSUk0VKIqvIMTVA6pZ9Px8Uxc3MhGIo83GaI=", "VfJhJsOfNN4DR3dwUaL8klI3uQNZEf3HnGkWMD388Dg=", new DateTime(2022, 1, 10, 10, 35, 47, 393, DateTimeKind.Local).AddTicks(6247), "Bret41" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 403, DateTimeKind.Local).AddTicks(9728), "Roxane43@hotmail.com", "nzPpkJnURpUzBnft3TUMGTRvVxSPj54PMpUPD+ALsGQ=", "NWENgJhLBlerGmXFWIcNH4/uHknX4Atr/19p/75mqS8=", new DateTime(2022, 1, 10, 10, 35, 47, 403, DateTimeKind.Local).AddTicks(9805), "Alfonso27" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2022, 1, 10, 10, 35, 47, 414, DateTimeKind.Local).AddTicks(1463), "Tyrel52@hotmail.com", "OMdvRxh0FE98Orb2m9nrxJk5XFD+NgiRjxFgwi6cqL4=", "X+dM+SzRya/JUcfwo2AxOa7sCoqeMJCCJVNtdukQ7Pw=", new DateTime(2022, 1, 10, 10, 35, 47, 414, DateTimeKind.Local).AddTicks(1532), "Amara26" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2022, 1, 10, 10, 35, 47, 424, DateTimeKind.Local).AddTicks(1356), "Deontae_Cassin18@hotmail.com", "DeOprPz1wPF5WbYLhoAmjJlRRPQtRh9QQY4SI1+0FWQ=", "z7zRBMcbAO5m7vk/LhavAKXuJ1qCm4QL539dKu6/Uag=", new DateTime(2022, 1, 10, 10, 35, 47, 424, DateTimeKind.Local).AddTicks(1394), "Araceli60" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2022, 1, 10, 10, 35, 47, 434, DateTimeKind.Local).AddTicks(4533), "Brooks_McKenzie94@yahoo.com", "0l8OlkOeAipiofblykz8pQjHuhqP6HtguSrQMx4L71Q=", "KLFumxQ/zjG67VAV6E4rApwap67ilc7NEOHKsCUYFlE=", new DateTime(2022, 1, 10, 10, 35, 47, 434, DateTimeKind.Local).AddTicks(4604), "August_Sipes" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2022, 1, 10, 10, 35, 47, 445, DateTimeKind.Local).AddTicks(666), "Nya86@yahoo.com", "OlkElZVPXw+hqhAWpW7CZJPXw5mhv68SEldqJZX6bVA=", "uV3FKl1xw4L7x3eiPISsDSj1DlDwye1+TF9gP/f0nJA=", new DateTime(2022, 1, 10, 10, 35, 47, 445, DateTimeKind.Local).AddTicks(747), "Mabel_Lind27" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2022, 1, 10, 10, 35, 47, 455, DateTimeKind.Local).AddTicks(1775), "Leslie.Thiel@gmail.com", "QqA5vOFhgFjrXlWc0hFhK5gOkKkNdmYb37Rj8LOgOR0=", "SXkeo1n8vF29sJNBnTBZynAZPXQ5n58gallWDbw4pDc=", new DateTime(2022, 1, 10, 10, 35, 47, 455, DateTimeKind.Local).AddTicks(1822), "Travon_Waters81" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2022, 1, 10, 10, 35, 47, 465, DateTimeKind.Local).AddTicks(6641), "Jed_Thompson@gmail.com", "Lq5Ods0QYcb9712vwTPQbRW5xWCjR+Ua4aJqPlTbTaU=", "3aOa4bWOOCK3Od1gf7x/h+jjHHwSVMl9UekivLLpXrY=", new DateTime(2022, 1, 10, 10, 35, 47, 465, DateTimeKind.Local).AddTicks(6711), "Richard_Towne" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2022, 1, 10, 10, 35, 47, 476, DateTimeKind.Local).AddTicks(568), "Michaela_Sawayn60@gmail.com", "UUkwgsLKNYoeWhA0ulhyWF1usZvIxbzL39VtXjjaSyA=", "LFKxiI9e8RN6TVP6Ouvfxdq31SjA+bu45xohawiefIU=", new DateTime(2022, 1, 10, 10, 35, 47, 476, DateTimeKind.Local).AddTicks(639), "Lisa10" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 10, 10, 35, 47, 486, DateTimeKind.Local).AddTicks(126), "ujMHKBAe6D0siXPPJWFjlcqwngGM8QStgv4iZdQAlbc=", "mLHDws6eKIWf6ODgT1FTzuLOLM1OpAT/Qv/AhADHAos=", new DateTime(2022, 1, 10, 10, 35, 47, 486, DateTimeKind.Local).AddTicks(126) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeletedAt",
                table: "Comments");

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 1, 11, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(3286), false, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(4320), 2 },
                    { 20, 20, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(6164), true, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(6172), 18 },
                    { 19, 16, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(6116), true, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(6124), 20 },
                    { 18, 11, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(6067), true, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(6076), 21 },
                    { 17, 15, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(6019), false, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(6028), 20 },
                    { 16, 3, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5968), false, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5976), 18 },
                    { 14, 20, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5867), false, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5876), 6 },
                    { 13, 14, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5807), true, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5820), 1 },
                    { 12, 12, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5685), true, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5694), 15 },
                    { 11, 19, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5633), false, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5642), 1 },
                    { 15, 5, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5919), false, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5927), 9 },
                    { 9, 14, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5528), true, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5537), 7 },
                    { 10, 5, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5578), false, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5586), 21 },
                    { 3, 10, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5186), false, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5196), 4 },
                    { 4, 19, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5240), false, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5249), 21 },
                    { 5, 10, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5290), true, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5299), 5 },
                    { 2, 18, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5113), true, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5129), 6 },
                    { 7, 1, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5422), false, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5432), 6 },
                    { 8, 3, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5478), false, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5487), 7 },
                    { 6, 17, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5340), false, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5349), 4 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Voluptatem fugit eum non accusantium occaecati beatae.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(284), 6, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(1173) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { "Fugiat saepe voluptatum fugiat autem voluptatum.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2073), new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2094) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Eum numquam dicta et odit officia esse hic.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2308), 19, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2322) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Ea dolore non itaque ullam.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2425), 11, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2438) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Corrupti est sed voluptas non et quisquam magnam ab et.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2560), 3, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2571) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Architecto unde ipsum.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2653), 2, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2664) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Ad fuga est similique quisquam voluptatem.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2757), 2, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2768) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Sapiente ut sed eius tenetur alias perferendis.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2865), 6, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2876) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Natus placeat eum laboriosam consequatur doloremque nam est numquam voluptas.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3056), 16, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3070) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Ut architecto dolore suscipit debitis eaque illum.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3174), 11, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3185) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Eos nostrum molestias iure rem dolor beatae veritatis et.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3292), 10, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3303) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Maxime saepe veniam labore sed inventore dolore.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3476), 15, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3491) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Velit ut ullam qui nihil libero dolores architecto voluptatum culpa.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3653), 19, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3666) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Quae eos facilis consectetur.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3818), 3, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3832) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Enim rerum vitae qui voluptas.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3951), 1, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3964) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Quas possimus inventore maxime architecto aliquam explicabo sed.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(4068), 7, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(4080) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Velit molestiae ex in et voluptatum.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(4176), 13, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(4187) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 14, "Vel necessitatibus et et.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(4272), new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(4283) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Enim necessitatibus similique illum aliquid et dolores.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(4386), 15, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(4397) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Veniam officiis asperiores tenetur quia natus eos.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(4577), 15, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(4591) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 668, DateTimeKind.Local).AddTicks(7212), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1052.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(3718) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5442), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/972.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5460) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5511), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/958.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5520) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5552), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/786.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5559) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5590), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/988.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5598) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5629), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/151.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5637) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5666), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1150.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5674) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5703), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/407.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5711) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5740), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/842.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5747) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5777), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/113.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5784) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5813), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/603.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5821) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5851), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/82.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5858) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5886), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/211.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5893) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5921), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/530.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5929) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5957), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/332.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5964) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5992), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1171.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(6000) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(6028), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/647.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(6035) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(6063), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1081.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(6071) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(6099), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/253.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(6106) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(6134), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/410.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(6142) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(5326), "https://picsum.photos/640/480/?image=96", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6287) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6664), "https://picsum.photos/640/480/?image=168", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6676) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6717), "https://picsum.photos/640/480/?image=591", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6726) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6760), "https://picsum.photos/640/480/?image=138", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6768) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6800), "https://picsum.photos/640/480/?image=498", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6808) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6841), "https://picsum.photos/640/480/?image=610", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6849) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6881), "https://picsum.photos/640/480/?image=902", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6889) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6921), "https://picsum.photos/640/480/?image=112", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6929) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7076), "https://picsum.photos/640/480/?image=1", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7088) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7129), "https://picsum.photos/640/480/?image=28", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7138) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7171), "https://picsum.photos/640/480/?image=574", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7179) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7211), "https://picsum.photos/640/480/?image=111", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7219) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7252), "https://picsum.photos/640/480/?image=615", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7259) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7292), "https://picsum.photos/640/480/?image=1051", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7299) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7331), "https://picsum.photos/640/480/?image=147", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7339) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7370), "https://picsum.photos/640/480/?image=247", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7378) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7409), "https://picsum.photos/640/480/?image=147", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7416) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7447), "https://picsum.photos/640/480/?image=786", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7454) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7485), "https://picsum.photos/640/480/?image=892", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7493) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7524), "https://picsum.photos/640/480/?image=1016", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7531) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(1144), false, 2, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(1152), 17 },
                    { 19, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(1096), false, 3, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(1105), 20 },
                    { 1, new DateTime(2022, 1, 9, 13, 32, 22, 993, DateTimeKind.Local).AddTicks(8059), true, 10, new DateTime(2022, 1, 9, 13, 32, 22, 993, DateTimeKind.Local).AddTicks(9079), 20 },
                    { 18, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(1046), true, 1, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(1055), 8 }
                });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 17, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(995), true, 14, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(1004), 4 },
                    { 16, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(947), true, 6, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(956), 4 },
                    { 14, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(847), true, 17, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(856), 14 },
                    { 13, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(796), false, 7, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(805), 15 },
                    { 12, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(746), false, 14, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(755), 3 },
                    { 11, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(697), true, 11, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(706), 1 },
                    { 10, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(644), false, 2, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(653), 7 },
                    { 15, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(897), true, 17, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(906), 21 },
                    { 8, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(543), true, 14, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(552), 17 },
                    { 7, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(492), false, 16, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(501), 3 },
                    { 6, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(441), false, 11, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(450), 14 },
                    { 5, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(390), true, 13, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(399), 13 },
                    { 9, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(594), true, 19, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(603), 5 },
                    { 4, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(336), false, 9, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(345), 19 },
                    { 3, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(283), false, 6, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(292), 18 },
                    { 2, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(186), true, 11, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(211), 6 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Magni culpa temporibus explicabo beatae praesentium dolor ipsa quibusdam aut.\nTempora et accusamus incidunt quidem maiores voluptate voluptas.\nRepellat ducimus velit qui.\nDolores autem voluptas repellat tenetur nobis quisquam et.\nNisi non provident aut quos iure adipisci.\nHarum aliquam sit blanditiis aut.", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(873), 36, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(1866) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "Temporibus qui hic nobis ipsum.\nIure earum libero.\nIpsa nulla nostrum accusantium voluptatem soluta cupiditate sit odit aut.\nVel alias harum illum sequi in magnam nesciunt alias at.\nAsperiores eum non dolor est.\nIpsa rem officia omnis quisquam magnam quia repellendus.", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(3596), 40, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(3623) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "nostrum", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(4318), 26, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(4341) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Velit est voluptatem accusamus perferendis distinctio.\nDolor nam sunt cumque.\nVoluptates explicabo qui architecto et reiciendis harum quam commodi.\nFacere nemo doloremque consectetur aspernatur fugit accusamus id.\nTemporibus ipsa repellat occaecati qui quis temporibus accusantium consequatur qui.\nIllo nesciunt dolorum recusandae illo quia asperiores numquam in.", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(4886), 40, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(4904) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "Aut aut voluptas dolorem error voluptatem repudiandae non quia.\nConsequatur voluptates in eligendi repellendus architecto tenetur dolorem ut mollitia.", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(5098), 33, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(5111) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Necessitatibus quia et laboriosam.\nOfficiis excepturi reiciendis assumenda voluptatum minima hic velit et molestiae.", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(5334), 38, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(5348) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Ratione harum ullam et. Aperiam explicabo natus nihil ratione. Quia maxime eligendi cum ut quaerat. Enim ipsa exercitationem recusandae consequatur numquam molestias. Facere culpa ut.", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(7262), 35, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(7291) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Doloribus aliquam qui modi perferendis eos omnis tenetur omnis fuga.\nTenetur sed molestias.\nCupiditate mollitia provident rerum libero fugiat non.\nVitae et velit minima sint voluptatem nobis distinctio quod.\nDolor cumque quam vitae aperiam.\nQui voluptatem nisi eum sit sed omnis.", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(7691), 39, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(7708) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "perferendis", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(7787), 23, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(7798) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, "enim", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(7862), 23, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(7873) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "Corporis vel culpa voluptatem non et sapiente ex.", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(8961), 33, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(8988) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "Error tenetur amet voluptate quos eum. Delectus consequatur minima aperiam quas aut perspiciatis quo. Aperiam est molestiae maiores nisi minus voluptas officiis.", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(9279), 40, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(9297) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Earum dolores odio eos similique in asperiores vel et.\nRerum autem est itaque provident perspiciatis possimus et.\nDolore laborum culpa omnis aut.", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(9523), 24, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(9536) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Voluptate beatae ipsam esse velit. Dolor mollitia facilis odio ut porro. Consequuntur consequatur voluptatibus adipisci sunt autem incidunt voluptas et quos.", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(9786), 24, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(9801) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "commodi", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(9872), 35, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(9883) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Et dolores et error harum autem sunt excepturi.\nRerum fugit deleniti aliquid provident nobis qui est ut.\nNon eaque sapiente maiores dolorem qui fugit dignissimos ab veritatis.\nEaque rerum ut.", new DateTime(2022, 1, 9, 13, 32, 22, 974, DateTimeKind.Local).AddTicks(120), 25, new DateTime(2022, 1, 9, 13, 32, 22, 974, DateTimeKind.Local).AddTicks(133) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Odio ut qui alias veniam deleniti eum consequuntur.", new DateTime(2022, 1, 9, 13, 32, 22, 974, DateTimeKind.Local).AddTicks(377), 21, new DateTime(2022, 1, 9, 13, 32, 22, 974, DateTimeKind.Local).AddTicks(393) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Voluptates hic expedita minus earum nihil.", new DateTime(2022, 1, 9, 13, 32, 22, 974, DateTimeKind.Local).AddTicks(507), 37, new DateTime(2022, 1, 9, 13, 32, 22, 974, DateTimeKind.Local).AddTicks(520) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Sint non cupiditate qui est et ipsum excepturi consequuntur laborum.", new DateTime(2022, 1, 9, 13, 32, 22, 974, DateTimeKind.Local).AddTicks(637), 26, new DateTime(2022, 1, 9, 13, 32, 22, 974, DateTimeKind.Local).AddTicks(649) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Qui non facere atque id eaque in.", new DateTime(2022, 1, 9, 13, 32, 22, 974, DateTimeKind.Local).AddTicks(753), 26, new DateTime(2022, 1, 9, 13, 32, 22, 974, DateTimeKind.Local).AddTicks(764) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2022, 1, 9, 13, 32, 22, 742, DateTimeKind.Local).AddTicks(5929), "Jarrell_Hackett@gmail.com", "vT6Y+LVYN8OyjAF1PRggK8VrLgLUyySrTXOL94pRJWQ=", "BLQ6IrZMojwtY9aWq1jo2xQxeHqe3MuDOvBEjgaSas4=", new DateTime(2022, 1, 9, 13, 32, 22, 742, DateTimeKind.Local).AddTicks(6915), "Jessy_Hartmann" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2022, 1, 9, 13, 32, 22, 753, DateTimeKind.Local).AddTicks(6336), "Nicolette9@hotmail.com", "6RH77xQQa9AIlAXRzJV5U+vDMXYG0bgrfqnqzwgL1bw=", "7EeXuAopzV+PnsNJjkyF0zOSmRRLQ/zYVhAuSbZBr+w=", new DateTime(2022, 1, 9, 13, 32, 22, 753, DateTimeKind.Local).AddTicks(6398), "Alia.Schmitt26" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2022, 1, 9, 13, 32, 22, 763, DateTimeKind.Local).AddTicks(9199), "Carrie.Dickens@yahoo.com", "9JRanlkf279gXemFBB4nwQMmPZefmO4QMOUieVTKy0M=", "1f/DiERc2DCKRxW7lIQIsCmhWm96i8fdn0fMPrv8RnY=", new DateTime(2022, 1, 9, 13, 32, 22, 763, DateTimeKind.Local).AddTicks(9278), "Eudora.Deckow69" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2022, 1, 9, 13, 32, 22, 774, DateTimeKind.Local).AddTicks(7389), "Jules94@hotmail.com", "sl0vy+u28zhivM1ygpDk8OlX3At5j+QinxrrtD64B5E=", "NqOvmoedEMbAlGGaVN3IUOBTUHIx7lhI8obXA6jU34k=", new DateTime(2022, 1, 9, 13, 32, 22, 774, DateTimeKind.Local).AddTicks(7473), "Hudson5" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2022, 1, 9, 13, 32, 22, 784, DateTimeKind.Local).AddTicks(9814), "Luella11@yahoo.com", "FQ7ANPcRd0ostVaTq6OOlKLfubAkcntOGmGPJCQEZx8=", "kRNVM+wK9rpGKBEiNiJsVUgUNToaCslsAhT5RlCQOrQ=", new DateTime(2022, 1, 9, 13, 32, 22, 784, DateTimeKind.Local).AddTicks(9874), "Alexandro83" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2022, 1, 9, 13, 32, 22, 795, DateTimeKind.Local).AddTicks(5890), "Jayne.Jones@hotmail.com", "Amicnd4dzTikEr6i1i+glZB7tWwfCIEVttNlzZK/Je8=", "Bs3BDZmtrZAcUhSJ8ZZLx4dt0syVIba7vm0BHWugALE=", new DateTime(2022, 1, 9, 13, 32, 22, 795, DateTimeKind.Local).AddTicks(5965), "Samir91" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2022, 1, 9, 13, 32, 22, 806, DateTimeKind.Local).AddTicks(874), "Garrett40@hotmail.com", "RWh4ouQDbNNe3B5DCjCLXYdFllYEqkIAfOyJ7I56Qkw=", "daA1ZEhYwCuAlq7WGIP7lDPkfH0054TMdpanW9wQ5nY=", new DateTime(2022, 1, 9, 13, 32, 22, 806, DateTimeKind.Local).AddTicks(957), "Kyra_Morar" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2022, 1, 9, 13, 32, 22, 816, DateTimeKind.Local).AddTicks(1173), "Lamont15@gmail.com", "fsaCPntnhM42f4sHKaS3DyqMMaa4jHY50QM5baGc294=", "GAAvy/vtVxmaV7TOKz0B4pNFAw4rMZA9wd865IFW7WA=", new DateTime(2022, 1, 9, 13, 32, 22, 816, DateTimeKind.Local).AddTicks(1202), "Joseph24" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2022, 1, 9, 13, 32, 22, 829, DateTimeKind.Local).AddTicks(6564), "Viola40@yahoo.com", "nwcxYKl/uO6QO7uD750f8wDh2pHnE5NOXAs5JmojcKw=", "nr2VkpZslNHHq690F9PCj2gSkZTGFK/FyxF+OJRAvk8=", new DateTime(2022, 1, 9, 13, 32, 22, 829, DateTimeKind.Local).AddTicks(6657), "Caterina_Smitham12" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2022, 1, 9, 13, 32, 22, 841, DateTimeKind.Local).AddTicks(702), "Riley_Bernhard@yahoo.com", "p55Ipj7yin7325M7n4ytoO+OyPzrn1275AAH6DZqSnA=", "CW+qImy+FXIlv0nyuAj0oLYHi2jMroBXPRGbQ9CFz2s=", new DateTime(2022, 1, 9, 13, 32, 22, 841, DateTimeKind.Local).AddTicks(785), "Ray.Parker17" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2022, 1, 9, 13, 32, 22, 851, DateTimeKind.Local).AddTicks(9891), "Easton22@hotmail.com", "IHqmlENu5BQx9y9fuI8SBpgwdcD9LJ/dN0TNZxOLIcI=", "VP/irsWGiDXKYxCxoIryqtW7z40sIVXKlw5BTlms8Gk=", new DateTime(2022, 1, 9, 13, 32, 22, 851, DateTimeKind.Local).AddTicks(9979), "Kamron_Stroman95" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2022, 1, 9, 13, 32, 22, 863, DateTimeKind.Local).AddTicks(1853), "Lillie_Tillman51@hotmail.com", "VBOFbWwdZTAR5rv9o0w/JDZw+rDo+HySek8AL+MRcIA=", "XjD8P0R/iF5UuenyPxFc3fBDzSjR42go2X7ieKwSwXc=", new DateTime(2022, 1, 9, 13, 32, 22, 863, DateTimeKind.Local).AddTicks(1939), "Hildegard.Goldner98" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 874, DateTimeKind.Local).AddTicks(4454), "Nova_Stroman48@hotmail.com", "wIVaNpXwXE9lx+o0ikkX82uz5x/EptCqq4X7VvZ0Mno=", "1E75Nl4J0Bfz2zNCJB+9AbG1+iHS2My3KOXRkL+r4og=", new DateTime(2022, 1, 9, 13, 32, 22, 874, DateTimeKind.Local).AddTicks(4547), "Ines_Kshlerin44" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2022, 1, 9, 13, 32, 22, 884, DateTimeKind.Local).AddTicks(8247), "Virginia.Langosh@yahoo.com", "HPW/uLgc1g11m6kN8RjviA2H0MfANACGhmIDsLgc4OU=", "BY8gYu1Z4QZ6Kdr9b1M4kXmfwwF555BIBHmfjOCgWWk=", new DateTime(2022, 1, 9, 13, 32, 22, 884, DateTimeKind.Local).AddTicks(8319), "Matilde28" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2022, 1, 9, 13, 32, 22, 894, DateTimeKind.Local).AddTicks(9659), "Hope.Hoeger@hotmail.com", "hhaiZnGLtfcH6r7SlJ63u5lsoQ9rjExLtZXWteiauvU=", "rMzLTQBVpqykxDV4nKzQlHvx1K6Pz/RBf7PqySnZhuY=", new DateTime(2022, 1, 9, 13, 32, 22, 894, DateTimeKind.Local).AddTicks(9703), "Afton.Prosacco" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2022, 1, 9, 13, 32, 22, 906, DateTimeKind.Local).AddTicks(106), "Sigmund_Gulgowski@hotmail.com", "nHOCb4vQqYipofnluGfLYvhEh4ogH2qpo+7UF6H3KCw=", "b6Jtm96PtlmcHJ8tUdEQJ0EAG6mptWsjvewojx9oVnA=", new DateTime(2022, 1, 9, 13, 32, 22, 906, DateTimeKind.Local).AddTicks(193), "Ora47" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2022, 1, 9, 13, 32, 22, 916, DateTimeKind.Local).AddTicks(2920), "Arnold46@yahoo.com", "JLbIF7gATCQDyWJyVKwAwjbxWTIKLz3u64o1AGFCCYs=", "5tcVoWcDYj4c/dMf8CVMG5iS0wCRol0yk+wDOzHEMyA=", new DateTime(2022, 1, 9, 13, 32, 22, 916, DateTimeKind.Local).AddTicks(2983), "Dulce_Collier" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2022, 1, 9, 13, 32, 22, 926, DateTimeKind.Local).AddTicks(4865), "Susan45@gmail.com", "czL1harWm3X+Mkh3Kk0S2oEr0ACUMg4FFeutrxn6snc=", "hEHa4GXQGVAqkb8E2jktXvw5khnkOaoH2dob5pDAIq4=", new DateTime(2022, 1, 9, 13, 32, 22, 926, DateTimeKind.Local).AddTicks(4915), "Fritz99" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2022, 1, 9, 13, 32, 22, 936, DateTimeKind.Local).AddTicks(7100), "Zoey9@gmail.com", "lSO6dxXoYmQNjS8c5fhDXt3uwWT/ThL3qzM3wfwtR2s=", "pUNNoP5zBmhUfr3slj0ZVRzG+oaBguIrDunIo+pDOsI=", new DateTime(2022, 1, 9, 13, 32, 22, 936, DateTimeKind.Local).AddTicks(7170), "Myrna24" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2022, 1, 9, 13, 32, 22, 946, DateTimeKind.Local).AddTicks(9455), "Andre.Greenholt27@yahoo.com", "KWiEDj5M9cjNlggv9gOIae850/N/8QzI7TgeiOpX7v0=", "D4TOzhtT6tu1sNw7vP+CvSGCkSp8a8we1EW4+afoprE=", new DateTime(2022, 1, 9, 13, 32, 22, 946, DateTimeKind.Local).AddTicks(9522), "Selmer.Nitzsche34" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2022, 1, 9, 13, 32, 22, 956, DateTimeKind.Local).AddTicks(9609), "8V/cty2pzJqfSJOkMQohBpTwdfWDcoQMCZMYT5/+P40=", "mZY+17Q+XJTylq9Qn8SKamJQWedjV07mJ6qEdFCseQs=", new DateTime(2022, 1, 9, 13, 32, 22, 956, DateTimeKind.Local).AddTicks(9609) });
        }
    }
}
