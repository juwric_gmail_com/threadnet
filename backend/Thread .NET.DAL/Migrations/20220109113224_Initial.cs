﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Images",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    URL = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Images", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AvatarId = table.Column<int>(type: "int", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Salt = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Images_AvatarId",
                        column: x => x.AvatarId,
                        principalTable: "Images",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Posts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AuthorId = table.Column<int>(type: "int", nullable: false),
                    PreviewId = table.Column<int>(type: "int", nullable: true),
                    Body = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Posts_Images_PreviewId",
                        column: x => x.PreviewId,
                        principalTable: "Images",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Posts_Users_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RefreshTokens",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Token = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Expires = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RefreshTokens", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RefreshTokens_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AuthorId = table.Column<int>(type: "int", nullable: false),
                    PostId = table.Column<int>(type: "int", nullable: false),
                    Body = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comments_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Comments_Users_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PostReactions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PostId = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    IsLike = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostReactions", x => x.Id);
                    table.UniqueConstraint("AK_PostReactions_PostId_UserId", x => new { x.PostId, x.UserId });
                    table.ForeignKey(
                        name: "FK_PostReactions_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PostReactions_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CommentReactions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CommentId = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    IsLike = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommentReactions", x => x.Id);
                    table.UniqueConstraint("AK_CommentReactions_CommentId_UserId", x => new { x.CommentId, x.UserId });
                    table.ForeignKey(
                        name: "FK_CommentReactions_Comments_CommentId",
                        column: x => x.CommentId,
                        principalTable: "Comments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CommentReactions_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Images",
                columns: new[] { "Id", "CreatedAt", "URL", "UpdatedAt" },
                values: new object[,]
                {
                    { 1, new DateTime(2022, 1, 9, 13, 32, 22, 668, DateTimeKind.Local).AddTicks(7212), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1052.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(3718) },
                    { 23, new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6717), "https://picsum.photos/640/480/?image=591", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6726) },
                    { 24, new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6760), "https://picsum.photos/640/480/?image=138", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6768) },
                    { 25, new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6800), "https://picsum.photos/640/480/?image=498", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6808) },
                    { 26, new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6841), "https://picsum.photos/640/480/?image=610", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6849) },
                    { 27, new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6881), "https://picsum.photos/640/480/?image=902", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6889) },
                    { 28, new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6921), "https://picsum.photos/640/480/?image=112", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6929) },
                    { 29, new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7076), "https://picsum.photos/640/480/?image=1", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7088) },
                    { 30, new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7129), "https://picsum.photos/640/480/?image=28", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7138) },
                    { 31, new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7171), "https://picsum.photos/640/480/?image=574", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7179) },
                    { 32, new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7211), "https://picsum.photos/640/480/?image=111", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7219) },
                    { 33, new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7252), "https://picsum.photos/640/480/?image=615", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7259) },
                    { 34, new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7292), "https://picsum.photos/640/480/?image=1051", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7299) },
                    { 35, new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7331), "https://picsum.photos/640/480/?image=147", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7339) },
                    { 36, new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7370), "https://picsum.photos/640/480/?image=247", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7378) },
                    { 37, new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7409), "https://picsum.photos/640/480/?image=147", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7416) },
                    { 38, new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7447), "https://picsum.photos/640/480/?image=786", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7454) },
                    { 39, new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7485), "https://picsum.photos/640/480/?image=892", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7493) },
                    { 22, new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6664), "https://picsum.photos/640/480/?image=168", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6676) },
                    { 40, new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7524), "https://picsum.photos/640/480/?image=1016", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(7531) },
                    { 21, new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(5326), "https://picsum.photos/640/480/?image=96", new DateTime(2022, 1, 9, 13, 32, 22, 677, DateTimeKind.Local).AddTicks(6287) },
                    { 19, new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(6099), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/253.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(6106) },
                    { 2, new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5442), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/972.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5460) },
                    { 3, new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5511), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/958.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5520) },
                    { 4, new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5552), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/786.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5559) },
                    { 5, new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5590), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/988.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5598) },
                    { 6, new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5629), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/151.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5637) },
                    { 7, new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5666), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1150.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5674) },
                    { 8, new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5703), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/407.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5711) },
                    { 9, new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5740), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/842.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5747) },
                    { 10, new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5777), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/113.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5784) },
                    { 11, new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5813), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/603.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5821) },
                    { 12, new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5851), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/82.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5858) },
                    { 13, new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5886), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/211.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5893) },
                    { 14, new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5921), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/530.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5929) },
                    { 15, new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5957), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/332.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5964) },
                    { 16, new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(5992), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1171.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(6000) },
                    { 17, new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(6028), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/647.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(6035) },
                    { 18, new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(6063), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1081.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(6071) },
                    { 20, new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(6134), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/410.jpg", new DateTime(2022, 1, 9, 13, 32, 22, 669, DateTimeKind.Local).AddTicks(6142) }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 21, null, new DateTime(2022, 1, 9, 13, 32, 22, 956, DateTimeKind.Local).AddTicks(9609), "test@gmail.com", "8V/cty2pzJqfSJOkMQohBpTwdfWDcoQMCZMYT5/+P40=", "mZY+17Q+XJTylq9Qn8SKamJQWedjV07mJ6qEdFCseQs=", new DateTime(2022, 1, 9, 13, 32, 22, 956, DateTimeKind.Local).AddTicks(9609), "testUser" });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "Id", "AuthorId", "Body", "CreatedAt", "DeletedAt", "PreviewId", "UpdatedAt" },
                values: new object[,]
                {
                    { 20, 21, "Qui non facere atque id eaque in.", new DateTime(2022, 1, 9, 13, 32, 22, 974, DateTimeKind.Local).AddTicks(753), null, 26, new DateTime(2022, 1, 9, 13, 32, 22, 974, DateTimeKind.Local).AddTicks(764) },
                    { 3, 21, "nostrum", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(4318), null, 26, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(4341) },
                    { 1, 21, "Magni culpa temporibus explicabo beatae praesentium dolor ipsa quibusdam aut.\nTempora et accusamus incidunt quidem maiores voluptate voluptas.\nRepellat ducimus velit qui.\nDolores autem voluptas repellat tenetur nobis quisquam et.\nNisi non provident aut quos iure adipisci.\nHarum aliquam sit blanditiis aut.", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(873), null, 36, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(1866) },
                    { 14, 21, "Voluptate beatae ipsam esse velit. Dolor mollitia facilis odio ut porro. Consequuntur consequatur voluptatibus adipisci sunt autem incidunt voluptas et quos.", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(9786), null, 24, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(9801) }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[,]
                {
                    { 17, 20, new DateTime(2022, 1, 9, 13, 32, 22, 916, DateTimeKind.Local).AddTicks(2920), "Arnold46@yahoo.com", "JLbIF7gATCQDyWJyVKwAwjbxWTIKLz3u64o1AGFCCYs=", "5tcVoWcDYj4c/dMf8CVMG5iS0wCRol0yk+wDOzHEMyA=", new DateTime(2022, 1, 9, 13, 32, 22, 916, DateTimeKind.Local).AddTicks(2983), "Dulce_Collier" },
                    { 5, 20, new DateTime(2022, 1, 9, 13, 32, 22, 784, DateTimeKind.Local).AddTicks(9814), "Luella11@yahoo.com", "FQ7ANPcRd0ostVaTq6OOlKLfubAkcntOGmGPJCQEZx8=", "kRNVM+wK9rpGKBEiNiJsVUgUNToaCslsAhT5RlCQOrQ=", new DateTime(2022, 1, 9, 13, 32, 22, 784, DateTimeKind.Local).AddTicks(9874), "Alexandro83" },
                    { 15, 19, new DateTime(2022, 1, 9, 13, 32, 22, 894, DateTimeKind.Local).AddTicks(9659), "Hope.Hoeger@hotmail.com", "hhaiZnGLtfcH6r7SlJ63u5lsoQ9rjExLtZXWteiauvU=", "rMzLTQBVpqykxDV4nKzQlHvx1K6Pz/RBf7PqySnZhuY=", new DateTime(2022, 1, 9, 13, 32, 22, 894, DateTimeKind.Local).AddTicks(9703), "Afton.Prosacco" },
                    { 12, 18, new DateTime(2022, 1, 9, 13, 32, 22, 863, DateTimeKind.Local).AddTicks(1853), "Lillie_Tillman51@hotmail.com", "VBOFbWwdZTAR5rv9o0w/JDZw+rDo+HySek8AL+MRcIA=", "XjD8P0R/iF5UuenyPxFc3fBDzSjR42go2X7ieKwSwXc=", new DateTime(2022, 1, 9, 13, 32, 22, 863, DateTimeKind.Local).AddTicks(1939), "Hildegard.Goldner98" },
                    { 16, 17, new DateTime(2022, 1, 9, 13, 32, 22, 906, DateTimeKind.Local).AddTicks(106), "Sigmund_Gulgowski@hotmail.com", "nHOCb4vQqYipofnluGfLYvhEh4ogH2qpo+7UF6H3KCw=", "b6Jtm96PtlmcHJ8tUdEQJ0EAG6mptWsjvewojx9oVnA=", new DateTime(2022, 1, 9, 13, 32, 22, 906, DateTimeKind.Local).AddTicks(193), "Ora47" },
                    { 20, 15, new DateTime(2022, 1, 9, 13, 32, 22, 946, DateTimeKind.Local).AddTicks(9455), "Andre.Greenholt27@yahoo.com", "KWiEDj5M9cjNlggv9gOIae850/N/8QzI7TgeiOpX7v0=", "D4TOzhtT6tu1sNw7vP+CvSGCkSp8a8we1EW4+afoprE=", new DateTime(2022, 1, 9, 13, 32, 22, 946, DateTimeKind.Local).AddTicks(9522), "Selmer.Nitzsche34" },
                    { 13, 15, new DateTime(2022, 1, 9, 13, 32, 22, 874, DateTimeKind.Local).AddTicks(4454), "Nova_Stroman48@hotmail.com", "wIVaNpXwXE9lx+o0ikkX82uz5x/EptCqq4X7VvZ0Mno=", "1E75Nl4J0Bfz2zNCJB+9AbG1+iHS2My3KOXRkL+r4og=", new DateTime(2022, 1, 9, 13, 32, 22, 874, DateTimeKind.Local).AddTicks(4547), "Ines_Kshlerin44" },
                    { 6, 14, new DateTime(2022, 1, 9, 13, 32, 22, 795, DateTimeKind.Local).AddTicks(5890), "Jayne.Jones@hotmail.com", "Amicnd4dzTikEr6i1i+glZB7tWwfCIEVttNlzZK/Je8=", "Bs3BDZmtrZAcUhSJ8ZZLx4dt0syVIba7vm0BHWugALE=", new DateTime(2022, 1, 9, 13, 32, 22, 795, DateTimeKind.Local).AddTicks(5965), "Samir91" },
                    { 2, 14, new DateTime(2022, 1, 9, 13, 32, 22, 753, DateTimeKind.Local).AddTicks(6336), "Nicolette9@hotmail.com", "6RH77xQQa9AIlAXRzJV5U+vDMXYG0bgrfqnqzwgL1bw=", "7EeXuAopzV+PnsNJjkyF0zOSmRRLQ/zYVhAuSbZBr+w=", new DateTime(2022, 1, 9, 13, 32, 22, 753, DateTimeKind.Local).AddTicks(6398), "Alia.Schmitt26" },
                    { 7, 11, new DateTime(2022, 1, 9, 13, 32, 22, 806, DateTimeKind.Local).AddTicks(874), "Garrett40@hotmail.com", "RWh4ouQDbNNe3B5DCjCLXYdFllYEqkIAfOyJ7I56Qkw=", "daA1ZEhYwCuAlq7WGIP7lDPkfH0054TMdpanW9wQ5nY=", new DateTime(2022, 1, 9, 13, 32, 22, 806, DateTimeKind.Local).AddTicks(957), "Kyra_Morar" },
                    { 19, 9, new DateTime(2022, 1, 9, 13, 32, 22, 936, DateTimeKind.Local).AddTicks(7100), "Zoey9@gmail.com", "lSO6dxXoYmQNjS8c5fhDXt3uwWT/ThL3qzM3wfwtR2s=", "pUNNoP5zBmhUfr3slj0ZVRzG+oaBguIrDunIo+pDOsI=", new DateTime(2022, 1, 9, 13, 32, 22, 936, DateTimeKind.Local).AddTicks(7170), "Myrna24" },
                    { 10, 9, new DateTime(2022, 1, 9, 13, 32, 22, 841, DateTimeKind.Local).AddTicks(702), "Riley_Bernhard@yahoo.com", "p55Ipj7yin7325M7n4ytoO+OyPzrn1275AAH6DZqSnA=", "CW+qImy+FXIlv0nyuAj0oLYHi2jMroBXPRGbQ9CFz2s=", new DateTime(2022, 1, 9, 13, 32, 22, 841, DateTimeKind.Local).AddTicks(785), "Ray.Parker17" },
                    { 11, 7, new DateTime(2022, 1, 9, 13, 32, 22, 851, DateTimeKind.Local).AddTicks(9891), "Easton22@hotmail.com", "IHqmlENu5BQx9y9fuI8SBpgwdcD9LJ/dN0TNZxOLIcI=", "VP/irsWGiDXKYxCxoIryqtW7z40sIVXKlw5BTlms8Gk=", new DateTime(2022, 1, 9, 13, 32, 22, 851, DateTimeKind.Local).AddTicks(9979), "Kamron_Stroman95" },
                    { 1, 6, new DateTime(2022, 1, 9, 13, 32, 22, 742, DateTimeKind.Local).AddTicks(5929), "Jarrell_Hackett@gmail.com", "vT6Y+LVYN8OyjAF1PRggK8VrLgLUyySrTXOL94pRJWQ=", "BLQ6IrZMojwtY9aWq1jo2xQxeHqe3MuDOvBEjgaSas4=", new DateTime(2022, 1, 9, 13, 32, 22, 742, DateTimeKind.Local).AddTicks(6915), "Jessy_Hartmann" },
                    { 14, 5, new DateTime(2022, 1, 9, 13, 32, 22, 884, DateTimeKind.Local).AddTicks(8247), "Virginia.Langosh@yahoo.com", "HPW/uLgc1g11m6kN8RjviA2H0MfANACGhmIDsLgc4OU=", "BY8gYu1Z4QZ6Kdr9b1M4kXmfwwF555BIBHmfjOCgWWk=", new DateTime(2022, 1, 9, 13, 32, 22, 884, DateTimeKind.Local).AddTicks(8319), "Matilde28" },
                    { 9, 4, new DateTime(2022, 1, 9, 13, 32, 22, 829, DateTimeKind.Local).AddTicks(6564), "Viola40@yahoo.com", "nwcxYKl/uO6QO7uD750f8wDh2pHnE5NOXAs5JmojcKw=", "nr2VkpZslNHHq690F9PCj2gSkZTGFK/FyxF+OJRAvk8=", new DateTime(2022, 1, 9, 13, 32, 22, 829, DateTimeKind.Local).AddTicks(6657), "Caterina_Smitham12" },
                    { 8, 4, new DateTime(2022, 1, 9, 13, 32, 22, 816, DateTimeKind.Local).AddTicks(1173), "Lamont15@gmail.com", "fsaCPntnhM42f4sHKaS3DyqMMaa4jHY50QM5baGc294=", "GAAvy/vtVxmaV7TOKz0B4pNFAw4rMZA9wd865IFW7WA=", new DateTime(2022, 1, 9, 13, 32, 22, 816, DateTimeKind.Local).AddTicks(1202), "Joseph24" },
                    { 3, 3, new DateTime(2022, 1, 9, 13, 32, 22, 763, DateTimeKind.Local).AddTicks(9199), "Carrie.Dickens@yahoo.com", "9JRanlkf279gXemFBB4nwQMmPZefmO4QMOUieVTKy0M=", "1f/DiERc2DCKRxW7lIQIsCmhWm96i8fdn0fMPrv8RnY=", new DateTime(2022, 1, 9, 13, 32, 22, 763, DateTimeKind.Local).AddTicks(9278), "Eudora.Deckow69" },
                    { 4, 13, new DateTime(2022, 1, 9, 13, 32, 22, 774, DateTimeKind.Local).AddTicks(7389), "Jules94@hotmail.com", "sl0vy+u28zhivM1ygpDk8OlX3At5j+QinxrrtD64B5E=", "NqOvmoedEMbAlGGaVN3IUOBTUHIx7lhI8obXA6jU34k=", new DateTime(2022, 1, 9, 13, 32, 22, 774, DateTimeKind.Local).AddTicks(7473), "Hudson5" },
                    { 18, 2, new DateTime(2022, 1, 9, 13, 32, 22, 926, DateTimeKind.Local).AddTicks(4865), "Susan45@gmail.com", "czL1harWm3X+Mkh3Kk0S2oEr0ACUMg4FFeutrxn6snc=", "hEHa4GXQGVAqkb8E2jktXvw5khnkOaoH2dob5pDAIq4=", new DateTime(2022, 1, 9, 13, 32, 22, 926, DateTimeKind.Local).AddTicks(4915), "Fritz99" }
                });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "Id", "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[,]
                {
                    { 14, 13, "Quae eos facilis consectetur.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3818), 3, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3832) },
                    { 5, 15, "Corrupti est sed voluptas non et quisquam magnam ab et.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2560), 3, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2571) },
                    { 15, 8, "Enim rerum vitae qui voluptas.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3951), 1, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3964) }
                });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 17, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(995), true, 14, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(1004), 4 },
                    { 8, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(543), true, 14, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(552), 17 },
                    { 19, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(1096), false, 3, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(1105), 20 },
                    { 18, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(1046), true, 1, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(1055), 8 },
                    { 12, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(746), false, 14, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(755), 3 }
                });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "Id", "AuthorId", "Body", "CreatedAt", "DeletedAt", "PreviewId", "UpdatedAt" },
                values: new object[,]
                {
                    { 10, 17, "enim", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(7862), null, 23, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(7873) },
                    { 15, 12, "commodi", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(9872), null, 35, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(9883) },
                    { 16, 20, "Et dolores et error harum autem sunt excepturi.\nRerum fugit deleniti aliquid provident nobis qui est ut.\nNon eaque sapiente maiores dolorem qui fugit dignissimos ab veritatis.\nEaque rerum ut.", new DateTime(2022, 1, 9, 13, 32, 22, 974, DateTimeKind.Local).AddTicks(120), null, 25, new DateTime(2022, 1, 9, 13, 32, 22, 974, DateTimeKind.Local).AddTicks(133) },
                    { 9, 20, "perferendis", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(7787), null, 23, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(7798) },
                    { 4, 20, "Velit est voluptatem accusamus perferendis distinctio.\nDolor nam sunt cumque.\nVoluptates explicabo qui architecto et reiciendis harum quam commodi.\nFacere nemo doloremque consectetur aspernatur fugit accusamus id.\nTemporibus ipsa repellat occaecati qui quis temporibus accusantium consequatur qui.\nIllo nesciunt dolorum recusandae illo quia asperiores numquam in.", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(4886), null, 40, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(4904) },
                    { 8, 6, "Doloribus aliquam qui modi perferendis eos omnis tenetur omnis fuga.\nTenetur sed molestias.\nCupiditate mollitia provident rerum libero fugiat non.\nVitae et velit minima sint voluptatem nobis distinctio quod.\nDolor cumque quam vitae aperiam.\nQui voluptatem nisi eum sit sed omnis.", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(7691), null, 39, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(7708) },
                    { 2, 7, "Temporibus qui hic nobis ipsum.\nIure earum libero.\nIpsa nulla nostrum accusantium voluptatem soluta cupiditate sit odit aut.\nVel alias harum illum sequi in magnam nesciunt alias at.\nAsperiores eum non dolor est.\nIpsa rem officia omnis quisquam magnam quia repellendus.", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(3596), null, 40, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(3623) },
                    { 18, 11, "Voluptates hic expedita minus earum nihil.", new DateTime(2022, 1, 9, 13, 32, 22, 974, DateTimeKind.Local).AddTicks(507), null, 37, new DateTime(2022, 1, 9, 13, 32, 22, 974, DateTimeKind.Local).AddTicks(520) },
                    { 7, 11, "Ratione harum ullam et. Aperiam explicabo natus nihil ratione. Quia maxime eligendi cum ut quaerat. Enim ipsa exercitationem recusandae consequatur numquam molestias. Facere culpa ut.", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(7262), null, 35, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(7291) },
                    { 12, 9, "Error tenetur amet voluptate quos eum. Delectus consequatur minima aperiam quas aut perspiciatis quo. Aperiam est molestiae maiores nisi minus voluptas officiis.", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(9279), null, 40, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(9297) },
                    { 11, 9, "Corporis vel culpa voluptatem non et sapiente ex.", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(8961), null, 33, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(8988) },
                    { 13, 3, "Earum dolores odio eos similique in asperiores vel et.\nRerum autem est itaque provident perspiciatis possimus et.\nDolore laborum culpa omnis aut.", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(9523), null, 24, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(9536) },
                    { 6, 3, "Necessitatibus quia et laboriosam.\nOfficiis excepturi reiciendis assumenda voluptatum minima hic velit et molestiae.", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(5334), null, 38, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(5348) },
                    { 19, 18, "Sint non cupiditate qui est et ipsum excepturi consequuntur laborum.", new DateTime(2022, 1, 9, 13, 32, 22, 974, DateTimeKind.Local).AddTicks(637), null, 26, new DateTime(2022, 1, 9, 13, 32, 22, 974, DateTimeKind.Local).AddTicks(649) },
                    { 5, 13, "Aut aut voluptas dolorem error voluptatem repudiandae non quia.\nConsequatur voluptates in eligendi repellendus architecto tenetur dolorem ut mollitia.", new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(5098), null, 33, new DateTime(2022, 1, 9, 13, 32, 22, 973, DateTimeKind.Local).AddTicks(5111) },
                    { 17, 18, "Odio ut qui alias veniam deleniti eum consequuntur.", new DateTime(2022, 1, 9, 13, 32, 22, 974, DateTimeKind.Local).AddTicks(377), null, 21, new DateTime(2022, 1, 9, 13, 32, 22, 974, DateTimeKind.Local).AddTicks(393) }
                });

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 13, 14, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5807), true, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5820), 1 },
                    { 15, 5, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5919), false, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5927), 9 },
                    { 10, 5, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5578), false, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5586), 21 },
                    { 9, 14, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5528), true, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5537), 7 },
                    { 17, 15, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(6019), false, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(6028), 20 }
                });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "Id", "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[,]
                {
                    { 12, 17, "Maxime saepe veniam labore sed inventore dolore.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3476), 15, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3491) },
                    { 7, 21, "Ad fuga est similique quisquam voluptatem.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2757), 2, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2768) },
                    { 6, 8, "Architecto unde ipsum.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2653), 2, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2664) },
                    { 16, 2, "Quas possimus inventore maxime architecto aliquam explicabo sed.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(4068), 7, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(4080) },
                    { 19, 12, "Enim necessitatibus similique illum aliquid et dolores.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(4386), 15, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(4397) },
                    { 20, 3, "Veniam officiis asperiores tenetur quia natus eos.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(4577), 15, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(4591) },
                    { 11, 19, "Eos nostrum molestias iure rem dolor beatae veritatis et.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3292), 10, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3303) },
                    { 10, 7, "Ut architecto dolore suscipit debitis eaque illum.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3174), 11, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3185) },
                    { 2, 21, "Fugiat saepe voluptatum fugiat autem voluptatum.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2073), 11, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2094) },
                    { 18, 14, "Vel necessitatibus et et.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(4272), 10, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(4283) },
                    { 17, 17, "Velit molestiae ex in et voluptatum.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(4176), 13, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(4187) },
                    { 8, 3, "Sapiente ut sed eius tenetur alias perferendis.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2865), 6, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2876) },
                    { 1, 20, "Voluptatem fugit eum non accusantium occaecati beatae.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(284), 6, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(1173) },
                    { 13, 1, "Velit ut ullam qui nihil libero dolores architecto voluptatum culpa.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3653), 19, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3666) },
                    { 3, 8, "Eum numquam dicta et odit officia esse hic.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2308), 19, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2322) },
                    { 4, 13, "Ea dolore non itaque ullam.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2425), 11, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(2438) },
                    { 9, 15, "Natus placeat eum laboriosam consequatur doloremque nam est numquam voluptas.", new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3056), 16, new DateTime(2022, 1, 9, 13, 32, 22, 983, DateTimeKind.Local).AddTicks(3070) }
                });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 7, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(492), false, 16, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(501), 3 },
                    { 1, new DateTime(2022, 1, 9, 13, 32, 22, 993, DateTimeKind.Local).AddTicks(8059), true, 10, new DateTime(2022, 1, 9, 13, 32, 22, 993, DateTimeKind.Local).AddTicks(9079), 20 },
                    { 13, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(796), false, 7, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(805), 15 },
                    { 20, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(1144), false, 2, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(1152), 17 },
                    { 10, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(644), false, 2, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(653), 7 },
                    { 11, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(697), true, 11, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(706), 1 },
                    { 6, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(441), false, 11, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(450), 14 },
                    { 2, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(186), true, 11, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(211), 6 },
                    { 5, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(390), true, 13, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(399), 13 },
                    { 16, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(947), true, 6, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(956), 4 },
                    { 3, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(283), false, 6, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(292), 18 },
                    { 9, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(594), true, 19, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(603), 5 },
                    { 15, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(897), true, 17, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(906), 21 },
                    { 4, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(336), false, 9, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(345), 19 },
                    { 14, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(847), true, 17, new DateTime(2022, 1, 9, 13, 32, 22, 994, DateTimeKind.Local).AddTicks(856), 14 }
                });

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 8, 3, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5478), false, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5487), 7 },
                    { 16, 3, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5968), false, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5976), 18 },
                    { 7, 1, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5422), false, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5432), 6 },
                    { 6, 17, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5340), false, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5349), 4 },
                    { 3, 10, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5186), false, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5196), 4 },
                    { 5, 10, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5290), true, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5299), 5 },
                    { 19, 16, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(6116), true, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(6124), 20 },
                    { 12, 12, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5685), true, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5694), 15 },
                    { 4, 19, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5240), false, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5249), 21 },
                    { 11, 19, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5633), false, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5642), 1 },
                    { 14, 20, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5867), false, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5876), 6 },
                    { 20, 20, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(6164), true, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(6172), 18 },
                    { 1, 11, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(3286), false, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(4320), 2 },
                    { 18, 11, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(6067), true, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(6076), 21 },
                    { 2, 18, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5113), true, new DateTime(2022, 1, 9, 13, 32, 23, 2, DateTimeKind.Local).AddTicks(5129), 6 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CommentReactions_UserId",
                table: "CommentReactions",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_AuthorId",
                table: "Comments",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_PostId",
                table: "Comments",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_PostReactions_UserId",
                table: "PostReactions",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_AuthorId",
                table: "Posts",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_PreviewId",
                table: "Posts",
                column: "PreviewId");

            migrationBuilder.CreateIndex(
                name: "IX_RefreshTokens_UserId",
                table: "RefreshTokens",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_AvatarId",
                table: "Users",
                column: "AvatarId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CommentReactions");

            migrationBuilder.DropTable(
                name: "PostReactions");

            migrationBuilder.DropTable(
                name: "RefreshTokens");

            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "Posts");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Images");
        }
    }
}
