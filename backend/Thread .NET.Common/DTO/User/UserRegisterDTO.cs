﻿using System.ComponentModel.DataAnnotations;

namespace Thread_.NET.Common.DTO.User
{
    public sealed class UserRegisterDTO : UserDTO
    {
        [Required]
        [StringLength(20, MinimumLength = 4)]
        public string Password { get; set; }
    }
}
