﻿using Microsoft.AspNetCore.SignalR;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Thread_.NET.Common.DTO.Post;

namespace Thread_.NET.BLL.Hubs
{
    public sealed class PostHub : Hub
    {
        public static ConcurrentDictionary<int, string> dict { get; set; } = new ConcurrentDictionary<int, string>();

        public static string GetConnectionId(int userId)
        {
            string result = "";
            dict.TryGetValue(userId, out result);
            return result;
        }

        public async Task Send(PostDTO post)
        {
            await Clients.All.SendAsync("NewPost", post);
        }

        public void RegisterConnection(int userId)
        {
            dict.TryAdd(userId, Context.ConnectionId);
        }
    }
}
