﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class PostService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;

        public PostService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper)
        {
            _postHub = postHub;
        }

        public async Task<ICollection<PostDTO>> GetAllPosts()
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                    .ThenInclude(user => user.Avatar)
                .Include(post => post.Comments.Where(c => c.DeletedAt == null))
                    .ThenInclude(comment => comment.Reactions)
                        .ThenInclude(rea => rea.User)
                .Include(post => post.Comments.Where(c => c.DeletedAt == null))
                    .ThenInclude(comment => comment.Author)
                         .ThenInclude(author => author.Avatar)
                .OrderByDescending(post => post.CreatedAt)
                .Where(post => post.DeletedAt == null)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<ICollection<PostDTO>> GetAllPosts(int userId)
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .Where(post => post.AuthorId == userId && post.DeletedAt == null) // Filter here
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<PostDTO> CreatePost(PostCreateDTO postDto)
        {
            var postEntity = _mapper.Map<Post>(postDto);

            _context.Posts.Add(postEntity);
            await _context.SaveChangesAsync();

            var createdPost = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .FirstAsync(post => post.Id == postEntity.Id);

            var createdPostDTO = _mapper.Map<PostDTO>(createdPost);
            await _postHub.Clients.All.SendAsync("NewPost", createdPostDTO);

            return createdPostDTO;
        }

        public async Task UpdatePost(PostUpdateDTO postDto)
        {
            var postEntity = await _context.Posts.Include(q => q.Preview)
                .FirstOrDefaultAsync(q => q.Id == postDto.Id);

            if (postEntity == null)
            {
                throw new NotFoundException(nameof(Post), postDto.Id);
            }

            postEntity.Body = postDto.Body;
            postEntity.Id = postDto.Id;
            postEntity.AuthorId = postDto.AuthorId;
            postEntity.UpdatedAt = DateTime.Now;

            if (postEntity.Preview.URL != postDto.PreviewImage)
            {
                postEntity.Preview = new Image { URL = postDto.PreviewImage };
            }

            _context.Posts.Update(postEntity);
            await _context.SaveChangesAsync();
        }

        public async Task DeletePost(int postId)
        {
            var postEntity = await _context.Posts.FirstOrDefaultAsync(u => u.Id == postId);
            if (postEntity == null)
            {
                throw new NotFoundException(nameof(Post), postId);
            }

            var image = await _context.Images.FirstOrDefaultAsync(u => u.Id == postEntity.PreviewId);
            if (image != null)
            {
                _context.Images.Remove(image);
            }

            postEntity.DeletedAt = DateTime.Now;
            await _context.SaveChangesAsync();
        }
    }
}
